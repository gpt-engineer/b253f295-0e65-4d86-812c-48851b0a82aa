// Todo App Logic
const todoForm = document.getElementById("todo-form");
const todoInput = document.getElementById("todo-input");
const todoList = document.getElementById("todo-list");

todoForm.addEventListener("submit", function (event) {
  event.preventDefault();
  const todoText = todoInput.value.trim();
  if (todoText !== "") {
    addTodoItem(todoText);
    todoInput.value = "";
  }
});

function addTodoItem(todoText) {
  const todoItem = document.createElement("li");
  todoItem.classList.add(
    "flex",
    "items-center",
    "justify-between",
    "bg-white",
    "px-4",
    "py-2",
    "border",
    "border-gray-300",
    "rounded-md",
    "slide-in", // Add class to trigger CSS animation
  );

  const todoTextElement = document.createElement("span");
  todoTextElement.textContent = todoText;
  todoItem.appendChild(todoTextElement);

  const todoActions = document.createElement("div");
  todoActions.classList.add("flex", "space-x-2");

  const completeButton = document.createElement("button");
  completeButton.innerHTML = '<i class="fas fa-check"></i>';
  completeButton.classList.add(
    "text-green-500",
    "hover:text-green-600",
    "focus:outline-none",
  );
  completeButton.addEventListener("click", function () {
    todoItem.classList.toggle("line-through");
  });
  todoActions.appendChild(completeButton);

  const deleteButton = document.createElement("button");
  deleteButton.innerHTML = '<i class="fas fa-trash"></i>';
  deleteButton.classList.add(
    "text-red-500",
    "hover:text-red-600",
    "focus:outline-none",
  );
  deleteButton.addEventListener("click", function () {
    todoItem.remove();
  });
  todoActions.appendChild(deleteButton);

  todoItem.appendChild(todoActions);

  todoList.appendChild(todoItem);
}
